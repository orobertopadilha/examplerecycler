package br.edu.unisep.examplerecycler.data

import java.io.Serializable

data class Contact(val name: String,
                   val email: String): Serializable
