package br.edu.unisep.examplerecycler

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import br.edu.unisep.examplerecycler.data.Contact
import br.edu.unisep.examplerecycler.databinding.ActivityNewContactBinding

class NewContactActivity : AppCompatActivity() {

    private val binding by lazy {
        ActivityNewContactBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        setupView()
    }

    private fun setupView() {
        binding.btnSave.setOnClickListener { save() }
        binding.etEmail.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                save()
                return@setOnEditorActionListener true
            }

            return@setOnEditorActionListener false
        }
    }

    private fun save() {
        val contact = Contact(binding.etName.text.toString(), binding.etEmail.text.toString())
        val intentResult = Intent().apply { putExtra("contact", contact) }

        setResult(RESULT_OK, intentResult)
        finish()
    }
}