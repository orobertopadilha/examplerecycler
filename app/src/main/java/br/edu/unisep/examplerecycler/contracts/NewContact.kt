package br.edu.unisep.examplerecycler.contracts

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.activity.result.contract.ActivityResultContract
import br.edu.unisep.examplerecycler.NewContactActivity
import br.edu.unisep.examplerecycler.data.Contact

class NewContact : ActivityResultContract<Unit, Contact>() {

    override fun createIntent(context: Context, input: Unit?) =
        Intent(context, NewContactActivity::class.java)

    override fun parseResult(resultCode: Int, intent: Intent?) =
        if (resultCode == Activity.RESULT_OK) {
            intent!!.getSerializableExtra("contact") as Contact
        } else {
            null
        }

}