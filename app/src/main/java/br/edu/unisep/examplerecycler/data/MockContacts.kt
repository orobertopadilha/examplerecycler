package br.edu.unisep.examplerecycler.data

fun generateMockContacts(): List<Contact> {
    val contacts = mutableListOf<Contact>()

    for (i in 0..20) {
        contacts.add(
            Contact("Contato Teste ${i}", "contato${i}@gmail.com")
        )
    }

    return contacts
}
