package br.edu.unisep.examplerecycler.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.edu.unisep.examplerecycler.data.Contact
import br.edu.unisep.examplerecycler.data.generateMockContacts
import br.edu.unisep.examplerecycler.databinding.ItemContactBinding

class ContactsAdapter : RecyclerView.Adapter<ContactsAdapter.ContactsViewHolder>() {

    var allContacts = listOf<Contact>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactsViewHolder {
        val itemBinding = ItemContactBinding.inflate(LayoutInflater.from(parent.context),
            parent, false)
        return ContactsViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ContactsViewHolder, position: Int) {
        holder.bind(allContacts[position])
    }

    override fun getItemCount() = allContacts.size

    class ContactsViewHolder(private val itemBinding: ItemContactBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(contact: Contact) {
            itemBinding.apply {
                tvName.text = contact.name
                tvEmail.text = contact.email
            }
        }
    }

    fun updateDataSet(contacts: List<Contact>) {
        this.allContacts = contacts
        notifyDataSetChanged()
    }

}