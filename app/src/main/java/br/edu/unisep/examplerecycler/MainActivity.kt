package br.edu.unisep.examplerecycler

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import br.edu.unisep.examplerecycler.adapter.ContactsAdapter
import br.edu.unisep.examplerecycler.contracts.NewContact
import br.edu.unisep.examplerecycler.data.Contact
import br.edu.unisep.examplerecycler.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private val binding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }

    private val adapter = ContactsAdapter()
    private val contacts = mutableListOf<Contact>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        setupView()
    }

    private fun setupView() {
        binding.rvContacts.adapter = adapter
        binding.rvContacts.layoutManager = LinearLayoutManager(
            this, LinearLayoutManager.VERTICAL, false)
        binding.rvContacts.addItemDecoration(
            DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        binding.btnAdd.setOnClickListener { openNewContact() }
    }

    private fun openNewContact() {
        newContactLauncher.launch(null)
    }

    private val newContactLauncher = registerForActivityResult(NewContact()) { contact ->
        contact?.let {
            contacts.add(it)
            adapter.updateDataSet(contacts)
        }
    }
}